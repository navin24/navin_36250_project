#ifndef __RTC_H
#define __RTC_H

#include "LPC17xx.h"

#define CCR_CLKEN	0
#define CCR_CLKRST	1

typedef struct rtc_time {
	uint32_t sec, min, hour;
	uint32_t day_of_month, month, year;
	uint32_t day_of_week, day_of_year;
}rtc_t;

void rtc_init(rtc_t *tm);
void rtc_set(rtc_t *tm);
void rtc_get(rtc_t *tm);

#endif

