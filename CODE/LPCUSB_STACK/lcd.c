#include "LPC17xx.h"
#include "lcd.h"

void lcd_init()
{
    LCD_DATA->FIODIR |= BV(D4) |BV(D5) |BV(D6) |BV(D7);
    LCD_COM->FIODIR |= BV(RS) |BV(EN) |BV(RW);
    lcd_busy();

    lcd_write_cmd(LCD_FUNC_SET);
    lcd_write_cmd(LCD_DISPLAY_ON);
    lcd_write_cmd(LCD_CLEAR);
    lcd_write_cmd(LCD_ENTRY_MODE);

}
void lcd_busy()
{
    LCD_DATA->FIODIR &= ~BV(D7);
    LCD_COM->FIOCLR |= BV(RS);
    LCD_COM->FIOSET |= BV(RW) | BV(EN);
    while((LCD_DATA->FIOPIN & BV(D7))!=0)
        ;
    LCD_COM->FIOCLR = BV(EN);
    LCD_DATA->FIODIR |=BV(D7);    

}
void lcd_write_cmd(uint8_t cmdval)
{
    uint8_t high_val =cmdval >>4 ,low_val =cmdval & 0x0F;

    LCD_COM->FIOCLR =BV(RS);

    lcd_write_nibble(high_val);
    lcd_write_nibble(low_val);

    lcd_busy();
    delay_ms(3);

    


}
void lcd_write_data(uint8_t cmdval)
{
    uint8_t high_val =cmdval >>4 ,low_val =cmdval & 0x0F;

    LCD_COM->FIOSET =BV(RS);

    lcd_write_nibble(high_val);
    lcd_write_nibble(low_val);

    lcd_busy();
    delay_ms(3);

    


}

void lcd_puts(uint8_t cmd,char data[])
{
    int i;
    lcd_write_cmd(cmd);
    for(i=0;data[i]!='\0';i++)
    {
        lcd_write_data(data[i]);
    }

}
void lcd_write_nibble(uint8_t cmd)
{
    
    LCD_COM->FIOCLR =BV(RW);
    LCD_DATA->FIOCLR |=BV(D4) |BV(D5) |BV(D6) |BV(D7);
    LCD_DATA->FIOSET |=((uint32_t)cmd)<<4;

    LCD_COM->FIOSET |= BV(EN);
    delay_ms(3);
    LCD_COM->FIOCLR = BV(EN);

}
