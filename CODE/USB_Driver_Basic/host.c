#include <linux/module.h> 		//	printk()
#include <linux/kernel.h> 		//	module loading
#include <linux/usb.h>			//	for usb 
#include <linux/uaccess.h>  	//	copy_from_user() and copy_to_user()
#include <linux/slab.h>			//	kmalloc()
#include <linux/errno.h>		//	for retval = -ENOMEM
#include <linux/spinlock.h> 	// spinklock_t
#include <linux/semaphore.h> 	// struct semaphore
#include <asm/atomic.h> 		// atomic 


#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX_PKT_SIZE 512   //apna yaha 64 tha


static struct usb_class_driver class;


/* Structure to hold all of our device specific stuff */
struct usb_dev {
	struct usb_device *usb_dev;
	struct usb_interface *interface;
	unsigned char *bulk_in_buffer;
	unsigned char *bulk_out_buffer;
	size_t bulk_in_size;
	__u8 bulk_in_endpointaddr;
	__u8 bulk_out_endpointaddr;
};



static struct usb_driver host_driver;


static int host_open(struct inode *inode, struct file *file)
{
	struct usb_dev *dev;
	struct usb_interface *interface;
	int subminor;
	int retval = 0;

	printk("Inside open....\n");

	subminor = iminor(inode);

	interface = usb_find_interface(&host_driver, subminor);
	if(!interface)
	{
		printk("can not find minor for device....\n");
		retval = -ENODEV;
		goto error1;
	}

	dev = usb_get_intfdata(interface);
	if(!dev){

		retval = -ENODEV;
		goto error1;
	}

	file->private_data = dev;

	error1:
		return retval;
}


static ssize_t host_read(struct file *file, char *buffer, size_t count, loff_t *offset){

	int retval;
	int cnt;
	struct usb_dev *dev;

	dev = file->private_data;

	printk("read :: inside read....\n");

	retval = usb_bulk_msg(dev->usb_dev,usb_rcvbulkpipe(dev->usb_dev, dev->bulk_in_endpointaddr),
								dev->bulk_in_buffer,MIN(dev->bulk_in_size, count),&cnt,5000);
	if(!retval){
	
		if(copy_to_user(buffer, dev->bulk_in_buffer, MIN(count, cnt))){
		
			retval = -EFAULT;
		}
		else {
		
			retval = count;
		}
	}

	return MIN(count, cnt);

}

//humara read sir ka code jaisa inka read ka code book jaisa...NEED TO CHANGE OR CHECK?

static ssize_t host_write(struct file *file, const char *buffer, size_t count, loff_t *offset){
	
	int retval;
	int cnt;

	struct usb_dev *dev;

	dev = file->private_data;

	printk("write :: inside write....\n");

	cnt = MIN(count, MAX_PKT_SIZE);

	if(copy_from_user(dev->bulk_out_buffer, buffer, MIN(count, MAX_PKT_SIZE))){
	
		return -EFAULT;
	}

		retval = usb_bulk_msg(dev->usb_dev, 
								usb_sndbulkpipe(dev->usb_dev, dev->bulk_out_endpointaddr), 
									dev->bulk_out_buffer,
										MIN(count, MAX_PKT_SIZE), 
											&cnt,
												5000);
		if(retval){
		
			printk("write :: can not write data....\n");
			return retval; 
		}

	return cnt;
}



static int host_close(struct inode *inode, struct file *file){

	struct usb_dev *dev;

	dev = file->private_data;

	if(dev == NULL){
		
		return -ENODEV;
	}

	printk("release :: usb released....\n");

	return 0;
}



static struct file_operations fops =
{
	.owner = THIS_MODULE,
	.open = host_open,
	.release = host_close,
	.read = host_read,
	.write = host_write,
};



static int host_probe(struct usb_interface *interface, const struct usb_device_id *id)
{

	//local structue to save device data
	struct usb_dev *dev;

	struct usb_host_interface *iface_desc;
	
	struct usb_endpoint_descriptor *endpoint;
	
	size_t buffer_size=0;
	
	int i, k;
	
	int retval = -ENOMEM;

	printk(KERN_INFO "Inside probe function\n");

	//kzalloc used to set int value to 0 and pointer to NULL after allocating memory (kmalloc not set this value)
	dev = kzalloc(sizeof(*dev), GFP_KERNEL);
	if(dev == NULL){  //(!dev) hai sir ke code mein
		printk(KERN_ERR "kzalloc failed\n");
		goto error;
	}

	//usb_device is save in local device structure
	dev->usb_dev =  usb_get_dev(interface_to_usbdev(interface)); //yaha usb_get_dev() aayega//

	iface_desc = interface->cur_altsetting;


	for(i = 0; i < iface_desc->desc.bNumEndpoints; ++i)
	{
		endpoint = &iface_desc->endpoint[i].desc;

		if(!dev->bulk_in_endpointaddr &&
		(endpoint->bEndpointAddress & USB_DIR_IN) &&
		((endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_BULK))
		{

			buffer_size = endpoint->wMaxPacketSize;
			dev->bulk_in_size = buffer_size;
			if( i == 0 )
			{
				dev->bulk_in_endpointaddr = endpoint->bEndpointAddress;
				dev->bulk_in_buffer = kmalloc(buffer_size, GFP_KERNEL);
				if(!dev->bulk_in_buffer)
				{

				printk(KERN_ERR "can not allocate buffer memory for in buffer0....\n");
					goto kmalloc_in_err;
				}
				else
				{
					printk(KERN_ERR "buffer in 0 allocated....\n");
				}
			}
		}

		if(!dev->bulk_out_endpointaddr &&
				!(endpoint->bEndpointAddress & USB_DIR_IN) &&
					((endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_BULK)){

			dev->bulk_out_endpointaddr = endpoint->bEndpointAddress;
			
			//alloc buffer
			dev->bulk_out_buffer = kmalloc(buffer_size, GFP_KERNEL);
			if(!dev->bulk_out_buffer)
			{
				printk(KERN_ERR "can not allocate buffer memory for out buffer....\n");
				goto kmalloc_in_err1;
			}
			else
				printk(KERN_ERR "buffer out allocated....\n");
		}
		printk(KERN_INFO "i ; %d\n", i);
	}

	/* check weather address of in and out bulk endpoints are allocated successfully.... */
	//if(!(dev->bulk_in_endpointaddr && dev->bulk_out_endpointaddr)){

	//	printk(KERN_ERR "can not find bulk in and bulk out end point address....\n");
	//	goto error;
	//}

	//printing data
	for (k = 0; k < iface_desc->desc.bNumEndpoints; k++)
	{
		endpoint = &iface_desc->endpoint[k].desc;

		printk(KERN_INFO "pd drive Endpoint [%d]->bEndpointAddress: 0x%02X\n", k, endpoint->bEndpointAddress);
		printk(KERN_INFO "pd drive Endpoint [%d]->bmAttributes: 0x%02X\n", k, endpoint->bmAttributes);
		printk(KERN_INFO "pd drive Endpoint [%d]->wMaxPacketSize: 0x%04X (%d)\n", k, endpoint->wMaxPacketSize, endpoint->wMaxPacketSize);
	}

	/* to save the local data structure associated with struct usb_interface.... */
	usb_set_intfdata(interface, dev);

	class.name = "usb/pd%d";  //	?
	class.fops = &fops;
	retval = usb_register_dev(interface, &class);
	if( retval )
	{
			//usb_set_intfdata(interface, NULL); //YEH NHI KIYA HAI CODE MEIN
			printk(KERN_ERR "Not able to get a minor for this device.");
			goto kmalloc_out_err;
	}
	else
		printk(KERN_INFO "Minor obtained: %d\n", interface->minor);

	return 0;
	
	kmalloc_out_err:
		kfree(dev->bulk_out_buffer);
		printk(KERN_INFO "probe() bulk_out_buffer freed\n");
	kmalloc_in_err1:
		kfree(dev->bulk_in_buffer);
		printk(KERN_INFO "probe() bulk_in_buffer freed\n");
	kmalloc_in_err:
		kfree(dev);
		printk(KERN_INFO "probe() dev freed\n");
	error:
		return retval;
}


//disconnect function
static void host_disconnect(struct usb_interface *interface)
{
	struct usb_dev *dev;
	int minor = interface->minor;

//	lock_kernel();
//	printk(KERN_INFO "disconnect() lock kernel\n");

	dev = usb_get_intfdata(interface);
	usb_set_intfdata(interface, NULL);

	printk(KERN_INFO "pd drive interface %d now disconnected.\n", interface->cur_altsetting->desc.bInterfaceNumber);
	usb_deregister_dev(interface, &class);	

//	unlock_kernel();
//	printk(KERN_INFO "disconnect() unlock kernel\n");


//	yaha dev->interface NULL kiyya hai




	kfree(dev->bulk_out_buffer);
	printk(KERN_INFO "disconnect() bulk_out_buffer free\n");

	
	kfree(dev->bulk_in_buffer);
	printk(KERN_INFO "disconnect() bulk_in_buffer freed\n");
	
	kfree(dev);
	dev->interface = NULL;
	printk(KERN_INFO "disconnect() dev freed\n");

	printk(KERN_INFO "USB/pd%d is now disconnectd\n", minor);
}





/* Table of devices that work with this driver */
//	1) Create a device id table
static struct usb_device_id id_table[] =  
{
	{ USB_DEVICE(0xffff, 0x0004) },
	{} /* Terminating entry */
};




//	2) Macro for exporting the Device id table
MODULE_DEVICE_TABLE (usb, id_table); 




//  3-4) declare probe and disconnect
static struct usb_driver host_driver =
{
	.name = "NASA",
	.probe = host_probe,
	.disconnect = host_disconnect,
	.id_table = id_table, // 3) id entry will be seen inside /sysfs
};






//init function
//5) Register this driver with the USB subsystem 
static int __init host_init(void)
{
	int result;
	result = usb_register(&host_driver);	//register
	if (result)
		printk(KERN_ERR "usb_register failed. Error number %d", result);
	else
		printk(KERN_ERR "usb_register() successfully registered driver %d", result);
	
	return result;
}



//exit function
/* Deregister this driver with the USB subsystem */
static void __exit host_exit(void)
{
	usb_deregister(&host_driver);	//deregister
	printk(KERN_ERR "usb_deregister() deregistered driver.");
}



module_init(host_init);
module_exit(host_exit);



MODULE_LICENSE("GPL");
MODULE_AUTHOR("N-A-S-A");
MODULE_DESCRIPTION("DESD E4 : Host device driver\n");












