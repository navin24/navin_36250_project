#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main() 
{
	int fd, ret;
	long newpos;
	char buf[64];

	fd = open("/dev/pd0", O_RDWR);
	if(fd < 0) {
		perror("failed to open device");
		_exit(1);
	}
	ret = read(fd, buf, sizeof(buf));
	printf("number of bytes read: %d\n", ret);
	buf[ret] = '\0';
	printf("data read: %s\n", buf);
	close(fd);

	getchar();

	fd = open("/dev/pd0", O_RDWR);
	if(fd < 0) {
		perror("failed to open device");
		_exit(1);
	}
	
	ret = write(fd, "hello desd\n", 11);
	printf("char written: %d\n", ret);
	close(fd);
	return 0;
}
