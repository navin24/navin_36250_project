# AVR assembly
* LDI
* Arithmetic instructions
	* ADD, SUB, MUL
	* ADC, ADIW, SBC, SBIW
	* MUL, MULS, MULSU
	* DEC, INC, CLR, COM
	* SREG flags
* LDS, STS
* IN, OUT
* Stack operations
	* setup SP
	* PUSH, POP
* Branching instructions
	* BRxx
	* RJMP
	* JMP
	* IJMP
* Logical operations
	* AND, ANDI
	* OR
	* EOR
	* CP, CPI
* Shift operations
	* LSL, LSR, ASR
	* ROR, ROL
* Function call instructions
	* RCALL
	* CALL
	* ICALL
	* RET
* Delay calculation
	* Instruction CPU cycles
	* Nested loops
* Bit instructions
	* SBI, CBI, SBIC, SBIS
	* SBR, CBR, SBRC, SBRS
	* "BRBS", "BRBC", BREQ, BRNE, ...
	* BSET, BCLR --> SREG bits set/clr
		* SEI, CLI --> SREG.I bit set/clear
		* SEC, CLC --> SREG.C bit set/clear
		* ...
* Addressing modes
	* Immediate
	* Register
	* Indirect
	* Flash direct
	* Flash Indirect


## Addressing modes
* Immediate mode / Single register mode
	* LDI R16, 0x00
	* CLR R0
	* LSR R1
	* ...
* Register mode / Register-register mode
	* ADD R0, R1
	* MOV R2, R3
	* CP R4, R6
	* ...
* Direct mode (RAM/IO register)
	* LDS R0, 0x0060
	* STS 0x0061, R1
	* OUT PORTA, R0
	* IN R1, PINA
* Indirect mode
	* LD/LDD, ST/STD
	* X+, -X, Y+, -Y, Y+q, Z+, -Z, Z+q

	```ASM
	; X = 0x0060
	; R0 = *X

	.DSEG
	num:	.BYTE 1

	.CSEG
	; ...
	
	main:
		; X = &num
		LDI XL, LOW(num)
		LDI XH, HIGH(num)
		; *X = 0
		CLR R1
		ST X, R1
		; R0 = *X
		LD R0, X
	```	

* Flash direct
	* .ORG 0000
	* Tells assembler to place next instruction at location 0000 in flash.
	* Branch/Function call instructions
		* JMP label / CALL label
			* label --> address of instruction in range 0 to 4M --> this label is replaced by assembler with address.

* Flash indirect
	* Branch/Function call indirect instructions
		* IJMP / ICALL
			* address of instruction taken from Z register.

			```ASM
			LDI ZL, LOW(label)
			LDI ZH, HIGH(label)
			IJMP
			```

	* place const data into flash & Read const data from flash

		```ASM
		.CSEG
		.ORG 0x0100
		str:	.DB	"SUNBEAM\0"

		main:
			LDI ZL, LOW(0x0100 * 2)
			LDI ZL, HIGH(0x0100 * 2)

			LPM R0, Z+
			LPM R0, Z
		```


# AVR C Programming
* AVR C Programming options
	* ATMEL AVR IDE / ATMEL Studio 6.x (based on VS studio) - Windows only
		* ATMEL assembler, C compiler, linker and tools
	* WinAVR - OpenSource - Windows only
		* GCC assembler, C compiler, linker and tools
		* IDE: Programmer's Notepad
		* Windows 8+ -- raise errors related to msys-1.0 dll (shell).
	* GCC AVR toolchain (Windows/Linux/Mac)
		* GCC assembler, C compiler, linker and tools
		* terminal> sudo apt install avr-gcc avr-binutils avrdude
	* VMLAB + WinAVR
		* WinAVR : avr-gcc toolchain
		* VMLAB : "AVR simulator" + avr-gcc frontend
* avrdude -- AVR programmer interface
	* Programmer: "TinyUSB", AVR109, ...














