#include "lcd.h"

void lcd_init(void)
{
	//1. set [data pins, control pins] io port pins as output.
	LCD_DATA_DIR |= LCD_DATA_MASK;
	LCD_DATA_CLR = LCD_DATA_MASK;
	LCD_CTRL_DIR |= LCD_CTRL_MASK;
	LCD_CTRL_CLR = LCD_CTRL_MASK;
	SW_DELAY_MS(200);
	
	//2. lcd func set: send instruction 0x28 [4-bit mode, 2 line, 5x8 font].
	lcd_write_byte(LCD_CMD, 0x28);
	//3. lcd display:  send instruction 0x0C [disp on, cursor off, blink off].
	lcd_write_byte(LCD_CMD, 0x0C);
	//4. lcd entry:    send instruction 0x06 [incr addr, no shift].
	lcd_write_byte(LCD_CMD, 0x06);
	//5. lcd clear:    send instruction 0x01 [display clear]
	lcd_write_byte(LCD_CMD, 0x01);
	
	SW_DELAY_MS(100);
}

void lcd_write_nibble(uint8_t rsflag, uint8_t data)
{
	//1. set RS=0 (instruction) or RS=1 (data)
	if(rsflag==LCD_CMD)
		LCD_CTRL_CLR = BV(LCD_RS);
	else
		LCD_CTRL_SET |= BV(LCD_RS);
	//2. RW=0
	LCD_CTRL_CLR = BV(LCD_RW);
	//3. Write nibble data
	LCD_DATA_CLR = LCD_DATA_MASK;
	LCD_DATA_SET = ((uint32_t)data) << LCD_D4;
	//4. EN=1 ... EN=0
	LCD_CTRL_SET |= BV(LCD_EN);
	SW_DELAY_MS(1);
	LCD_CTRL_CLR = BV(LCD_EN);
}

void lcd_busy_wait(void)
{
	//0. Make LCD data port pin as input.
	LCD_DATA_DIR &= ~LCD_DATA_MASK;
	//1. RS=0
	LCD_CTRL_CLR = BV(LCD_RS);	
	//2. RW=1 //3. EN=1
	LCD_CTRL_SET |= BV(LCD_RW) | BV(LCD_EN);
	//4. read LCD data & check D7 bit; if bit=1, repeat (this line).
	while((LCD_DATA_PIN & BV(LCD_D7)) != 0);
	//5. EN=0	//6. RW=0
	LCD_CTRL_CLR = BV(LCD_RW) | BV(LCD_EN);
	//7. Make LCD data port pin as output.
	LCD_DATA_DIR |= LCD_DATA_MASK;
}

void lcd_write_byte(uint8_t rsflag, uint8_t data)
{
    //1. write high nibble
	lcd_write_nibble(rsflag, data >> 4);
    //2. write low nibble
	lcd_write_nibble(rsflag, data & 0x0F);
    //3. check busy flag	
	lcd_busy_wait();
	
	SW_DELAY_MS(3);
}

void lcd_puts(uint8_t line, char str[])
{
	int i;
	//1. set line address:
	lcd_write_byte(LCD_CMD, line);
	//2. write char data one by one.
	for(i=0; str[i]!='\0'; i++)
		lcd_write_byte(LCD_DATA, str[i]);
}




