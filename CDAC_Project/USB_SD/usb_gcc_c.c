#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

typedef struct packet{
	int data;
	char buf[60];	
}packet_t;

void read_data(int fd);
void write_data(int fd);


int main() 
{
	int choice , fd;
	fd = open("/dev/pd0", O_RDWR);
	if(fd < 0) {
		perror("failed to open device");
		_exit(1);
	}
do
{
	printf("\n0.Exit.\n1.Reading Data.\n2.Writing Data.\n");
	printf("\nPlease Enter a choice	:	");
	scanf("%d",&choice);

	switch(choice)
	{
		case 0:
			break;
		case 1:
			read_data(fd);
			break;
		case 2:
			write_data(fd);
			break;
		default:
			printf("Invalid Choice.\n");
	}
}while(choice!=0);
	close(fd);
	return 0;
}

	
	
void read_data(int fd)
{
	//-------------File transfer from SD card to User Space--------
	int ret,ret1,fd1;
	FILE *fp;

	char buf[64];
	packet_t d1;
	ret = read(fd, buf, sizeof(buf));
	printf("\nReading File Name.\n");

	
	fp = fopen(buf,"w");
	if(!fp) {
		perror("failed to open file");
		close(fd);
		_exit(1);
	}
	printf("Reading Data from SDcard\n");	
	while(1)
	{
		ret = read(fd, buf, sizeof(buf));
		if(strcmp(buf,"$NASA$")!=0)
		{
			fwrite(buf,1,sizeof(buf),fp);
		}
		else
			break;
	}
	printf("File Read from SDcard Completely.\n");
	fclose(fp);
}
void write_data(int fd)
{
	//-------------File transfer from User Space to SD card--------------
	int ret,ret1,fd1;
	FILE *fp;

	char buf[64];
	packet_t d1;

	fd1= open("/home/sunbeam/Desktop/Finale/USB_SD/day03.md",O_RDONLY);
	if(fd1 < 0) {
		perror("failed to open file/n");
		_exit(1);
	}

	d1.data =0;// for name related packet
	strcpy(d1.buf,"day03.md");
	ret = write(fd,&d1,sizeof(d1));
	printf("\nWriting Data into SDcard\n");
	d1.data=1;// for data related packet
	while((ret =read(fd1,d1.buf,sizeof(d1.buf)))>0)
	{
		ret1=write(fd,&d1,sizeof(d1));
		memset(d1.buf,' ',sizeof(d1.buf));
	}

	d1.data =2;
	d1.buf[0]='\0';
	printf("File successfully sent to SDcard\n");	
	
	ret1=write(fd,&d1,sizeof(d1));
	close(fd1);
}

