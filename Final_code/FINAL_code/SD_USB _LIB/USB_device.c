
#include <stdio.h>
#include <string.h>
#include "lcd.h"
#include "sd.h"
#include "usbapi.h"
#include "usbdebug.h"
#include "usbstruct.h"
#include "fat_filelib.h"
#include "media_api.h"

#include "LPC17xx.h"

#define usbMAX_SEND_BLOCK		( 20 / portTICK_PERIOD_MS )
#define usbBUFFER_LEN			( 20 )

#define INCREMENT_ECHO_BY 1
#define BAUD_RATE	115200

#define INT_IN_EP		0x81
#define BULK_OUT_EP		0x05
#define BULK_IN_EP		0x82

#define MAX_PACKET_SIZE	64

#define LE_WORD(x)		((x)&0xFF),((x)>>8)

static unsigned char abBulkBuf[64];
//static unsigned char abClassReqData[8];


// forward declaration of interrupt handler
void USBIntHandler(void);

#define BULK_IN_1_EP		0x82	//refer pg.no.110 of Axelson, Field: bEndpointAddress
#define BULK_IN_2_EP		0x85	
#define BULK_OUT_1_EP		0x08	


static const unsigned char abDescriptors[] = {

/* Device descriptor */
	0x12,					// Length of descriptor
	DESC_DEVICE,       		// Type of descriptor
	LE_WORD(0x0200),		// bcdUSB
	0x00,              		// bDeviceClass
	0x00,              		// bDeviceSubClass
	0x00,              		// bDeviceProtocol
	MAX_PACKET_SIZE0,  		// bMaxPacketSize
	LE_WORD(0xFFFF),		// idVendor
	LE_WORD(0x0004),		// idProduct
	LE_WORD(0x0100),		// bcdDevice
	0x01,              		// iManufacturer
	0x02,              		// iProduct
	0x03,              		// iSerialNumber
	0x01,              		// bNumConfigurations

// Configuration Descriptor
	0x09,
	DESC_CONFIGURATION,
	LE_WORD(0x27),  		// wTotalLength
	0x01,  					// bNumInterfaces
	0x01,  					// bConfigurationValue
	0x00,  					// iConfiguration
	0x80,  					// bmAttributes		//bus powered
	0x32,  					// bMaxPower	(100mA Current)

// Interface Descriptor
	0x09,
	DESC_INTERFACE,
	0x00,  		 			// bInterfaceNumber
	0x00,   				// bAlternateSetting
	0x03,   				// bNumEndPoints
	0xFF,   				// bInterfaceClass
	0x00,   				// bInterfaceSubClass
	0x00,   				// bInterfaceProtocol
	0x00,   				// iInterface

// Bulk IN 1 Endpoint
	0x07,
	DESC_ENDPOINT,
	BULK_IN_1_EP,					// bEndpointAddress
	0x02,   					// bmAttributes = Bulk
	LE_WORD(MAX_PACKET_SIZE),	// wMaxPacketSize
	0x00,						// bInterval

// Bulk IN 2 Endpoint
	0x07,
	DESC_ENDPOINT,
	BULK_IN_2_EP,				// bEndpointAddress
	0x02,   					// bmAttributes = Bulk
	LE_WORD(MAX_PACKET_SIZE),	// wMaxPacketSize
	0x00,						// bInterval

// Bulk OUT 1 Endpoint
	0x07,
	DESC_ENDPOINT,
	BULK_OUT_1_EP,				// bEndpointAddress
	0x02,   					// bmAttributes = Bulk
	LE_WORD(MAX_PACKET_SIZE),	// wMaxPacketSize
	0x00,						// bInterval

// string descriptors
	0x04,
	DESC_STRING,
	LE_WORD(0x0409),

// manufacturer string
	0x14,
	DESC_STRING,
	'P', 0, 'A', 0, 'S', 0, 'S' , 0, ' ', 0, 'L', 0, 'T', 0, 'D', 0, '.', 0,

// product string
	0x10,
	DESC_STRING,
	'U', 0, 'S', 0, 'B', 0, ' ', 0, 'A', 0, 'R', 0, 'M', 0,


// serial number string
	0x12,
	DESC_STRING,
	'0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '1', 0,

// terminator
	0
};

/**
	Local function to handle incoming bulk data
		
	@param [in] bEP
	@param [in] bEPStatus
 */
int flag=0;
FL_FILE *f1=NULL;
static void BulkOut(unsigned char bEP, unsigned char bEPStatus)
{
	//lcd_puts(LCD_LINE1, "SUN INFO");
	int iLen;
	char *size;
	FL_FILE *file;
	int l =sizeof(abBulkBuf);
	char arr[l+1];
	//long lHigherPriorityTaskWoken = pdFALSE;

	( void ) bEPStatus;
	

	// get data from USB into intermediate buffer
	iLen = USBHwEPRead(bEP, abBulkBuf, sizeof(abBulkBuf));
	lcd_puts(LCD_LINE2, (char *)abBulkBuf);
 	// Create function
	if(flag==0){
		sprintf(arr,"%s%s","/",abBulkBuf);
		file = fl_fopen(arr,"w");	
		if (file)
   	 	{
	 	 flag=1;
		 f1=file;
		// fl_fclose(file);	 
       	 	}
   	 	else
      		  printf("ERROR: Create file failed\n");
	}
	else{
		size = strchr(abBulkBuf,'\0');
		if(size!= NULL){	
		
		 fl_fwrite(abBulkBuf,strlen(abBulkBuf)-1,1, f1);
		 fl_fclose(f1);
		 flag=0;
		 f1=NULL;
	}
		else if(size== NULL)
		 fl_fwrite(abBulkBuf, sizeof(abBulkBuf),1, f1);
		//lcd_puts(LCD_LINE2, "Sunbeam3");
    }
}


/**
	Local function to handle outgoing bulk data
		
	@param [in] bEP
	@param [in] bEPStatus
 */
static void BulkIn(unsigned char bEP, unsigned char bEPStatus)
{
	
	lcd_puts(LCD_LINE2, "SUNBEAM INFOTECH");
	
	( void ) bEPStatus;
	
	// send over USB
	
	USBHwEPWrite(bEP, abBulkBuf, 64);
	
}

//void USBIntHandler(void)
void USB_IRQHandler(void)
{
	USBHwISR();
}

int main(void)
{
	FL_FILE *file;
	unsigned char data1[] = "Sunbeam Pune";
	char data2[16];
	SystemInit();
	// initialise stack
	USBInit();
	lcd_init();
	lcd_puts(LCD_LINE1, "Sunbeam");
	//ret = if_initInterface(NULL);
	//sprintf((char *)abBulkBuf, "ret val : %02d", ret);
	//lcd_puts(LCD_LINE2, (char *)abBulkBuf);
	
	//--------------------------------------------------------------------------
	media_init();
	lcd_puts(LCD_LINE2, "Sunbeam1");
	// Initialise File IO Library
    fl_init();
	
    // Attach media access functions to library
    if (fl_attach_media(media_read, media_write) != FAT_INIT_OK)
    {
		//lcd_puts(LCD_LINE2, "Sunbeam2");
        printf("ERROR: Media attach failed\n");
        return; 
    }
	
    // List root directory
    //fl_listdirectory("/");

    // Create File
  /*  file = fl_fopen("/file1.txt", "w");
    if (file)
    {
        // Write some data
        
        if (fl_fwrite(data1, sizeof(data1),1, file) != sizeof(data1))
            printf("ERROR: Write file failed\n");
		//lcd_puts(LCD_LINE2, "Sunbeam3");
    }
    else
        printf("ERROR: Create file failed\n");

    // Close file
    fl_fclose(file);*/
	//-----------------------------------------------
/*	file = fl_fopen("/file3.txt", "r");
    if (file)
    {
        // Write some data
        lcd_puts(LCD_LINE2, "Sunbeam4");
        if (fl_fread(data2, sizeof(data2),1, file) != sizeof(data2))
            printf("ERROR: Write file failed\n");
		lcd_puts(LCD_LINE2, data2);
    }
    else
        printf("ERROR: Create file failed\n");

    // Close file
    fl_fclose(file);
	

	

    // Delete File
    if (fl_remove("/file1.txt") < 0)
        printf("ERROR: Delete file failed\n");

    // List root directory
    //fl_listdirectory("/");
*/
    fl_shutdown();
	
	
	
	
	//-------------------------------------------------------------------
	// register descriptors
	USBRegisterDescriptors(abDescriptors);

	
	// register endpoint handlers
//	USBHwRegisterEPIntHandler(INT_IN_EP, NULL);
	USBHwRegisterEPIntHandler(BULK_IN_1_EP, BulkIn);
	USBHwRegisterEPIntHandler(BULK_OUT_1_EP, BulkOut);
	
	// enable bulk-in interrupts on NAKs
	USBHwNakIntEnable(INACK_BI);

	DBG("Starting USB communication\n");
	//lcd_init();
	
	
	//NVIC_SetPriority( USB_IRQn, configUSB_INTERRUPT_PRIORITY );
	NVIC_EnableIRQ( USB_IRQn );
		
	// connect to bus
		
	DBG("Connecting to USB bus\n");
	USBHwConnect(TRUE);

	// echo any character received (do USB stuff in interrupt)
	for( ;; );
}

