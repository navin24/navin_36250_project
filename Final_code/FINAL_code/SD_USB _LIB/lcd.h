#ifndef __LCD_H
#define __LCD_H

#include "LPC17xx.h"

#define LCD_DATA_SET LPC_GPIO2->FIOSET
#define LCD_DATA_CLR LPC_GPIO2->FIOCLR
#define LCD_DATA_PIN LPC_GPIO2->FIOPIN
#define LCD_DATA_DIR LPC_GPIO2->FIODIR
 
#define LCD_CTRL_SET LPC_GPIO1->FIOSET
#define LCD_CTRL_CLR LPC_GPIO1->FIOCLR
#define LCD_CTRL_PIN LPC_GPIO1->FIOPIN
#define LCD_CTRL_DIR LPC_GPIO1->FIODIR

#define LCD_D4	4
#define LCD_D5	5
#define LCD_D6	6
#define LCD_D7	7

#define LCD_RS	24
#define LCD_RW	23
#define LCD_EN	22
#define LCD_BL	21

#define LCD_DATA_MASK (BV(LCD_D4)|BV(LCD_D5)|BV(LCD_D6)|BV(LCD_D7))
#define LCD_CTRL_MASK (BV(LCD_RS)|BV(LCD_RW)|BV(LCD_EN)|BV(LCD_BL))

#define LCD_LINE1	0x80
#define LCD_LINE2	0xC0

#define LCD_CMD		0
#define LCD_DATA	1

void lcd_init(void);
void lcd_write_nibble(uint8_t rsflag, uint8_t data);
void lcd_busy_wait(void);
void lcd_write_byte(uint8_t rsflag, uint8_t data);
void lcd_puts(uint8_t line, char str[]);

#endif





