#ifndef __MEDIA_H
#define __MEDIA_H

int media_init(void);
int media_read(unsigned long sector, unsigned char *buffer, unsigned long sector_count);
int media_write(unsigned long sector, unsigned char *buffer, unsigned long sector_count);

#endif