#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/usb.h>
#include <linux/module.h>

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX_PKT_SIZE 512

#define USB_MINOR_BASE 192
#define USB_VENDOR_ID 0xffff
#define USB_PRODUCT_ID 0x0004

static struct usb_device_id usb_table[] = {

	{USB_DEVICE(USB_VENDOR_ID, USB_PRODUCT_ID)},
	{}
};
MODULE_DEVICE_TABLE(usb, usb_table);

struct usb_dev {

	struct usb_device *udev;
	struct usb_interface *interface;
	unsigned char *bulk_in_buffer;
	size_t bulk_in_size;
	__u8 bulk_in_endpointaddr;
	__u8 bulk_out_endpointaddr;

};

static struct usb_driver usb_drive;

static int usb_open(struct inode *inode, struct file *file){

	struct usb_dev *dev;
	struct usb_interface *interface;
	int subminor;
	int retval = 0;

	printk("open :: inside open....\n");

	subminor = iminor(inode);

	interface = usb_find_interface(&usb_drive, subminor);
	if(!interface){
	
		printk("can not find minor for device....\n");
		retval = -ENODEV;
		goto error;
	}

	dev = usb_get_intfdata(interface);
	if(!dev){
	
		retval = -ENODEV;
		goto error;
	}

	file->private_data = dev;

	error:
		return retval;
}

static ssize_t usb_read(struct file *file, char *buffer, size_t count, loff_t *offset){

	int retval;
	int cnt;
	struct usb_dev *dev;

	dev = file->private_data;

	printk("read :: inside read....\n");

	retval = usb_bulk_msg(dev->udev,usb_rcvbulkpipe(dev->udev, dev->bulk_in_endpointaddr),
								dev->bulk_in_buffer,
									MIN(dev->bulk_in_size, count),
										&cnt,
											5000);
	if(!retval){
	
		if(copy_to_user(buffer, dev->bulk_in_buffer, MIN(count, cnt))){
		
			retval = -EFAULT;
		}
		else {
		
			retval = count;
		}
	}

	return MIN(count, cnt);
}

static ssize_t usb_write(struct file *file, const char *buffer, size_t count, loff_t *offset){

	int retval;
	int cnt;

	struct usb_dev *dev;

	dev = file->private_data;

	printk("write :: inside write....\n");

	cnt = MIN(count, MAX_PKT_SIZE);

	if(copy_from_user(dev->bulk_in_buffer, buffer, MIN(count, MAX_PKT_SIZE))){
	
		return -EFAULT;
	}

		retval = usb_bulk_msg(dev->udev, 
								usb_sndbulkpipe(dev->udev, dev->bulk_out_endpointaddr), 
									dev->bulk_in_buffer,
										MIN(count, MAX_PKT_SIZE), 
											&cnt,
												5000);
		if(retval){
		
			printk("write :: can not write data....\n");
			return retval; 
		}

	return cnt;
}

static int usb_release(struct inode *inode, struct file *file){

	struct usb_dev *dev;

	dev = file->private_data;

	if(dev == NULL){
		
		return -ENODEV;
	}

	printk("release :: usb released....\n");

	return 0;
}

static const struct file_operations usb_fops = {

	.owner = THIS_MODULE,
	.read = usb_read,
	.write = usb_write,
	.open = usb_open,
	.release = usb_release,
};

static struct usb_class_driver usb_class = {

	.name = "usb%d",
	.fops = &usb_fops,
	.minor_base = USB_MINOR_BASE,
};

/*
 	usb class driver info in order to get a minor number from the usb core,
  	and to have the device registered with the driver core
*/


static int usb_probe(struct usb_interface *interface, const struct usb_device_id *id){

	/* structure declaration is in same file..... */
	struct usb_dev *dev;

	/*
		structure declaration is in usb.h...
			struct usb_host_interface{
				struct usb_interface_decriptor desc;
				int extralen;
				unsigned char *extra;
				struct usb_host_endpoint *endpoint;
				char *string;
			}
	*/
	struct usb_host_interface *iface_desc;

	/*
		This structure is member of struct usb_host_endpoint{} 				--> in usb.h -->

		actual declaration of structure is in --> usb.h --> 				linux/usb/ch9.h....
			struct usb_endpoint_descriptor{
				bLength;
				bDescriptorType;
				bEndpointAddress;
				bmAttributes;
				wMaxPacketSize;
				bInterval;

			}
	*/
	struct usb_endpoint_descriptor *endpoint;
	size_t buffer_size;
	int i;
	int retval = -ENOMEM;

	printk("probe :: inside probe....\n");

	dev = kzalloc(sizeof(*dev), GFP_KERNEL);
	if(!dev){
		
		dev_err(&interface->dev, "out of memory....\n");
		goto error;
	}

	dev->udev = usb_get_dev(interface_to_usbdev(interface));

	/*
		udev --> member of struct usb_derive{}
			type --> struct usb_device{}

		interface --> member of struct usb_drive{}
			type --> struct usb_interfave{}
	*/
	
	/*
	dev->udev = usb_get_dev(interface_to_usbdev(interface));
	dev->interface = interface;
	*/

	/*
		cur_altsetting --> 
			member of struct usb_interface{} --> in usb.h

			type --> struct usb_host_interface{}...		(pointer to structure)
	*/
	iface_desc = interface->cur_altsetting;

	/* bNumEndpoints --> member of struct usb_interface_descriptor{} */
	
	for(i = 0; i < iface_desc->desc.bNumEndpoints; ++i){
	
		endpoint = &iface_desc->endpoint[i].desc;

		/*
			checking for in_endpoint....
			and if found setting up in endpoint information....
			
			- bulk_in_endpointaddr --> struct usb_driver{};
			- bEndpointAddress --> struct usb_endpoint_descriptor{} (ch9.h);
			- USB_DIR_IN --> 0x80;
			- USB_ENDPOINT_XFERTYPE_MASK --> 0x03;
			- USB_ENDPOINT_XFER_BULK --> 2

			instead of following alternative way is....
				if (!dev->bulk_in_endpointAddr &&
						usb_endpoint_is_bulk_in(endpoint)){}

				inbuile function in ch9.h...
		*/	
		if(!dev->bulk_in_endpointaddr &&
				(endpoint->bEndpointAddress & USB_DIR_IN) &&
					((endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_BULK)){

			/* bulk_in_endpoint... */

			/*
				setting up Max packet size...
				wMaxPacketSize --> member of struct usb_endpoint_descriptor{};
			*/

			buffer_size = endpoint->wMaxPacketSize;
			dev->bulk_in_size = buffer_size;
			dev->bulk_in_endpointaddr = endpoint->bEndpointAddress;// bit 0:3 -> EP number,bit 4:6 -> Reserved,bit 7-> EP address

			/*
				setting up bulk_in endpoint address....
				bEndpointAddress --> struct usb_endpoint_descriptor{}....;
			*/


			/* allocating memory for bulk_in endpoint buffer from kernel space.... */
			dev->bulk_in_buffer = kmalloc(buffer_size, GFP_KERNEL);
			if(!dev->bulk_in_buffer){
			
				dev_err(&interface->dev, "can not allocate buffer memory....\n");
				goto error;
			}
		}

		/*
			check for out_endpoint....
			and if then setting up out_endpoint information....

			another methode....
				if (!dev->bulk_out_endpointAddr &&
						 usb_endpoint_is_bulk_out(endpoint))
		*/
		if(!dev->bulk_out_endpointaddr &&
				!(endpoint->bEndpointAddress & USB_DIR_IN) && 
					((endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_BULK)){
			
			/* out_endpoint found.... */
			
			/*
				setting up bulk_out endpoint address....
				bEndpointAddress --> struct usb_endpoint_descriptor{}....(ch9.h);
			*/
			dev->bulk_out_endpointaddr = endpoint->bEndpointAddress;
		}//end of if....

	}//end of for loop...

/* check weather address of in and out bulk endpoints are allocated successfully.... */

	if(!(dev->bulk_in_endpointaddr && dev->bulk_out_endpointaddr)){
	
		dev_err(&interface->dev, "can not find bulk in and bulk out end point address....\n");
		goto error;
	}

/* to save the local data structure associated with struct usb_interface.... */
	usb_set_intfdata(interface, dev);

/* 
			after setting up all in_endpoint and out_endpoint register device using usb_register_dev.... 
			
			the diff between usb_register and usb_register_dev is....
				usb_register() --> registers struct usb_driver{}....
				while,
				usb_register_dev() --> registers interface params and struct usb_dev{}....
		*/
	retval = usb_register_dev(interface, &usb_class);
	if(retval){
	
		dev_err(&interface->dev, "not able to get minor number....\n");
		goto error;
	}

	/* printing device information on terminal.... */
	dev_info(&interface->dev, "usb device is created :: minor no = %d....\n", interface->minor);
	return 0;

error:
	return retval;
}

static void usb_disconnect(struct usb_interface *interface){

	struct usb_dev *dev;
	int minor = interface->minor;

/* to avoid race condition form usb_open().... */
	//lock_kernel();

	/* remove all information from interface descriptor....*/
	dev = usb_get_intfdata(interface);
	usb_set_intfdata(interface, NULL);

	/* deregister the device by deallocating minor number.... */
	usb_deregister_dev(interface, &usb_class);

	dev->interface = NULL;

/* unlocking kernel again....*/
	//unlock_kernel();

	/* printing device status.... */

	dev_info(&interface->dev, "usb device is disconnected :: minor = %d....\n", minor);
}

static struct usb_driver usb_drive = {

	.name = "usb_host_driver",
	.id_table = usb_table,
	.probe = usb_probe,
	.disconnect = usb_disconnect,
	
};

static int __init usb_init(void){

	int retval;

/*
		Registration of struct usb_driver{} to USB core....
			usb_register(&usb_driver);

		another way is,
			module_usb_driver(usb_drive);
	*/
	retval = usb_register(&usb_drive);
	if(retval < 0){
		
		printk("usb registration failed....\n");
		return -1;
	}

	printk("usb driver module register....\n");
	return retval;
}

static void __exit usb_exit(void){

	/* deregistering usb device.... */
	usb_deregister(&usb_drive);
	printk("usb driver module deregister....\n");
}

module_init(usb_init);
module_exit(usb_exit);
/*
	way of registering struct usb_driver{}...
		module_usb_driver(usb_drive);

		in this case there is no need of writing module_init()....
		and module_exit();
*/

MODULE_LICENSE("GPL");
MODULE_AUTHOR("wharish05@gmail.com");
MODULE_DESCRIPTION("usb driver");
