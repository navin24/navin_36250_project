#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/usb.h>
#include <linux/mutex.h>

#define USB_VENDOR_ID 0xffff
#define USB_PRODUCT_ID 0x0004
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#define USB_MINOR_BASE 192
#define MAX_PKT_SIZE 512

static struct usb_device_id usb_table [] = {
	
	{ USB_DEVICE(USB_VENDOR_ID, USB_PRODUCT_ID) },
	{}
};
MODULE_DEVICE_TABLE(usb, usb_table);

struct usb_dev {

	struct usb_device *udev;
	struct usb_interface *interface;
//	struct usb_anchor submitted;
	struct urb *bulk_in_urb;

	unsigned char *bulk_in_buffer;
	size_t bulk_in_size;
	size_t bulk_in_filled;
	size_t bulk_in_copied;

	uint8_t bulk_in_endpointaddr;
	uint8_t bulk_out_endpointaddr;

	int err;
//	bool on_read;

	struct mutex io_mutex;
};

static struct usb_driver usb_drive;


static int usb_open(struct inode *inode, struct file *file){

	int retval = 0;
	int subminor;
	struct usb_dev *dev;
	struct usb_interface *interface;

	printk("open() :: inside open function....\n");
	subminor = iminor(inode);

	interface = usb_find_interface(&usb_drive, subminor);
	if(!interface){
		
		printk(KERN_INFO "can not find minor number....:: %d\n", subminor);
		retval = -ENODEV;
		goto exit;
	}

	dev = usb_get_intfdata(interface);
	if(!dev){
		
		retval = -ENODEV;
		goto exit;
	}
	printk("open :: minor number :: %d\n", subminor);

	//usb_autopm_get_interface(interface);
	file->private_data = dev;

exit:
	return retval;
}

static void usb_read_bulk_callback(struct urb *urb){

	struct usb_dev *dev;

	printk("usb_read_bulk_callback() :: inside callback function....\n");
	dev = urb->context;

	if(urb->status){
	
		if(!(urb->status == -ENOENT ||
			urb->status == -ECONNRESET ||
			urb->status == -ESHUTDOWN)){
			
			dev_err(&dev->interface->dev,
							"%s - nonzero write bulk status received: %d\n", __func__, urb->status);
		}

		dev->err = urb->status;
	} else {
	
		dev->bulk_in_filled = urb->actual_length;
	}

}

/*
static int usb_read_io(struct usb_dev *dev, size_t length){

	int retval;

	printk("usb_read_io() :: inside read io function....\n");

	usb_fill_bulk_urb(dev->bulk_in_urb, 
						dev->udev,
						usb_rcvbulkpipe(dev->udev, 
							dev->bulk_in_endpointaddr),
						dev->bulk_in_buffer,
						MIN(dev->bulk_in_size, length),
						usb_read_bulk_callback,
						dev);

	dev->bulk_in_filled = 0;
	dev->bulk_in_copied = 0;

	retval = usb_submit_urb(dev->bulk_in_urb, GFP_KERNEL);
	if(retval < 0){
		dev_err(&dev->interface->dev,"%s - failed submitting read urb, error %d\n", __func__, retval);
		retval = (retval == -ENOMEM) ? retval : -EIO;
	}
	return retval;
}
*/

static ssize_t usb_read(struct file *file, char *buffer, size_t length, loff_t *offset){

	int retval = 0;
	int cnt;
	struct usb_dev *dev;
	dev = file->private_data;
	
	printk("read() :: inside read call....\n");

	cnt = MIN(length, MAX_PKT_SIZE);
	//	if we cannot read at all, return EOF 
	if(!dev->bulk_in_urb || !length){
		
		printk("1st if block --> bulk_in_urb fails....\n");
		return 0;
	}


	/* no concurrent readers */
	retval = mutex_lock_interruptible(&dev->io_mutex);
	if(retval < 0){
		
		printk("mutex fails....\n");
		return retval;
	}

/*
	// disconnect() was called 
	if(!dev->interface){
		
		printk("3rd if block --> interface fails....\n");
		retval = -ENODEV;
		goto exit;
	}
*/
//retry:
//	if(dev->bulk_in_filled){
	
//		size_t available = dev->bulk_in_filled - dev->bulk_in_copied;
//		size_t chunk = min(available, length);

/*
		if(!available){
				
			retval = usb_read_io(dev, length);
			if(retval < 0){
				
				goto exit;
			} else {
				
				goto retry;
			}
		}
*/
//		printk("inside if :: bulk_in_filled....\n");
			
//		retval = usb_read_io(dev, length);

	usb_fill_bulk_urb(dev->bulk_in_urb, 
						dev->udev,
						usb_rcvbulkpipe(dev->udev, 
							dev->bulk_in_endpointaddr),
						dev->bulk_in_buffer,
						MIN(dev->bulk_in_size, length),
						usb_read_bulk_callback,
						dev);

//	dev->bulk_in_filled = 0;
//	dev->bulk_in_copied = 0;

	retval = usb_submit_urb(dev->bulk_in_urb, GFP_KERNEL);
	if(retval < 0){
		dev_err(&dev->interface->dev,"%s - failed submitting read urb, error %d\n", __func__, retval);
		retval = (retval == -ENOMEM) ? retval : -EIO;
	}
		
		if(!retval){
			if(copy_to_user(buffer,
								dev->bulk_in_buffer, 
								MIN(length, cnt))){
				retval = -EFAULT;	
			} else {
				
				retval = length;
			}
		}

//			dev->bulk_in_copied += MIN(length, cnt);

//			if(available <length){
			
//				usb_read_io(dev, length - chunk);
//			}
//	} else {
		
//		retval = usb_read_io(dev, length);
//		if(retval < 0){
				
//			goto exit;
//		} else {
				
//			goto retry;
//		}
//	}

//exit:
	mutex_unlock(&dev->io_mutex);
	return retval;
}

static int usb_release(struct inode *inode, struct file *file){
	
	struct usb_dev *dev;

	dev = file->private_data;
	if(dev == NULL){
		
		return -ENODEV;
	}

	printk("release() ::usb released....\n");

	return 0;
}

static const struct file_operations usb_fops = {

	.owner = THIS_MODULE,
	.read = usb_read,
	.open = usb_open,
	.release = usb_release,
};

static struct usb_class_driver usb_class = {
	
	.name = "usb%d",
	.fops = &usb_fops,
	.minor_base = USB_MINOR_BASE,
};

static int usb_driver_probe(struct usb_interface *interface, const struct usb_device_id *id){

/* structure declaration is in same file..... */
	struct usb_dev *dev;
	/*
		structure declaration is in usb.h...
			struct usb_host_interface{
				struct usb_interface_decriptor desc;
				int extralen;
				unsigned char *extra;
				struct usb_host_endpoint *endpoint;
				char *string;
			}
	*/

	struct usb_host_interface *inter_desc;
	/*
		This structure is member of struct usb_host_endpoint{} --> in usb.h -->

			actual declaration of structure is in --> usb.h --> linux/usb/ch9.h....
				struct usb_endpoint_descriptor{
					bLength;
					bDescriptorType;
					bEndpointAddress;
					bmAttributes;
					wMaxPacketSize;
					bInterval;
				}
	*/

	struct usb_endpoint_descriptor *endpoint;
	size_t buffer_size;
	int i;
	int retval = -ENOMEM;

	printk(KERN_INFO "Inside probe....\n");

	dev = kzalloc(sizeof(*dev), GFP_KERNEL);
	if(!dev){
		dev_err(&interface->dev, "can not allocate memory....\n");
		goto error;
	}

	dev->udev = usb_get_dev(interface_to_usbdev(interface));
	//other initializtions....

	/*
		udev --> member of struct usb_derive{}
			type --> struct usb_device{}

		interface --> member of struct usb_drive{}
			type --> struct usb_interfave{}
	*/
															
	/*
		dev->udev = usb_get_dev(interface_to_usbdev(interface));
			dev->interface = interface;
	*/

	/*
		cur_altsetting --> member of struct usb_interface{} --> in usb.h
			type --> struct usb_host_interface{}...(pointer to structure)
	*/

	mutex_init(&dev->io_mutex);
	inter_desc = interface->cur_altsetting;
	
	/* bNumEndpoints --> member of struct usb_interface_descriptor{} */

	for(i = 0; i < inter_desc->desc.bNumEndpoints;i++){
	
		endpoint = &inter_desc->endpoint[i].desc;
		
		/*
			checking for in_endpoint....
				and if found setting up in endpoint information....
											
				- bulk_in_endpointaddr --> struct usb_driver{};
				- bEndpointAddress --> struct usb_endpoint_descriptor{} (ch9.h);
				- USB_DIR_IN --> 0x80;
				- USB_ENDPOINT_XFERTYPE_MASK --> 0x03;
				- USB_ENDPOINT_XFER_BULK --> 2

			instead of following alternative way is....
				if (!dev->bulk_in_endpointAddr &&
					usb_endpoint_is_bulk_in(endpoint)){}

			inbuild function in ch9.h...
		*/

		if(!dev->bulk_in_endpointaddr && 
				(endpoint->bEndpointAddress & USB_DIR_IN) &&
					(endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) == 
						USB_ENDPOINT_XFER_BULK){
		
			/* bulk_in_endpoint... */

			/*
				setting up Max packet size...
				wMaxPacketSize --> member of struct usb_endpoint_descriptor{}...(ch9.h); 
			*/
			buffer_size = endpoint->wMaxPacketSize;
			dev->bulk_in_size = buffer_size;
			
			/*
				setting up bulk_in endpoint address....
				bEndpointAddress --> struct usb_endpoint_descriptor{}....(ch9.h);
			*/
			dev->bulk_in_endpointaddr = endpoint->bEndpointAddress;
			
			/* allocating memory for bulk_in endpoint buffer from kernel space.... */
			dev->bulk_in_buffer = kmalloc(buffer_size, GFP_KERNEL);
			if(!dev->bulk_in_buffer){
				
				dev_err(&interface->dev, "No more memory available to allocate....\n");
				goto error;
			}

			/*	allocate urb from kernel space....	*/
			dev->bulk_in_urb = usb_alloc_urb(0, GFP_KERNEL);
			if(!dev->bulk_in_urb){
				dev_err(&interface->dev, "Could not allocate bulk_in_urb\n");
				goto error;
			}

			printk(KERN_INFO "bulk_in_endpointaddr :: %d....\n", dev->bulk_in_endpointaddr);
		}
		
		/*
			check for out_endpoint....
				and if then setting up out_endpoint information....

			another methode....
				if (!dev->bulk_out_endpointAddr &&
					usb_endpoint_is_bulk_out(endpoint))
		*/
		if(!dev->bulk_out_endpointaddr &&
				!(endpoint->bEndpointAddress & USB_DIR_IN) && 
					((endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_BULK)){
			
			/* out_endpoint found.... */
							
			/*
				setting up bulk_out endpoint address....
					bEndpointAddress --> struct usb_endpoint_descriptor{}....(ch9.h);
			*/
			dev->bulk_out_endpointaddr = endpoint->bEndpointAddress;

			printk(KERN_INFO "bulk_out_endpointaddr :: %d....\n", dev->bulk_out_endpointaddr);
		}
	}

	/* check weather address of in and out bulk endpoints are allocated successfully.... */
	if(!(dev->bulk_in_endpointaddr && dev->bulk_out_endpointaddr)){
	
		dev_err(&interface->dev, "Address :: bulk_in and bulk_out are not available....\n");
		goto error;
	}

	/* to save the local data structure associated with struct usb_interface.... */
	usb_set_intfdata(interface, dev);

	/* 
		after setting up all in_endpoint and out_endpoint register device using usb_register_dev.... 
								
		the diff between usb_register and usb_register_dev is....
			usb_register() --> registers struct usb_driver{}....
		while,
			usb_register_dev() --> registers interface params and struct usb_dev{}....
	*/
	retval = usb_register_dev(interface, &usb_class);
	if(retval){
		
		dev_err(&interface->dev, "Unable to get minor number for this device....\n");
		usb_set_intfdata(interface, NULL);
		goto error;
	}

	
	/* printing device information on terminal.... */
	dev_info(&interface->dev, "USB device is now attached :: %d....\n", interface->minor);

	printk(KERN_INFO "device is successfully created....\n");

	error:
		return retval;
}

static void usb_driver_disconnect(struct usb_interface *interface){

	struct usb_dev *dev;
	int minor = interface->minor;


	/* remove all information from interface descriptor....*/
	dev = usb_get_intfdata(interface);
	usb_set_intfdata(interface, NULL);

	/* deregister the device by deallocating minor number.... */
	usb_deregister_dev(interface, &usb_class);

	/* to avoid race condition form usb_open().... */
	//lock_kernel();
	
	mutex_lock(&dev->io_mutex);
	dev->interface = NULL;
	mutex_unlock(&dev->io_mutex);
	/* unlocking kernel again....*/
	//unlock_kernel();

	/* printing device status.... */
	dev_info(&interface->dev, "USB device is now disconnected :: %d....\n", minor);
}

static struct usb_driver usb_drive = {

	.name = "usbCode",
	.id_table = usb_table,
	.probe = usb_driver_probe,
	.disconnect = usb_driver_disconnect,
};

static int __init usb_driver_init(void){

	int retval;
	/*
		Registration of struct usb_driver{} to USB core....
			usb_register(&usb_driver);

		another way is,
			module_usb_driver(usb_drive);
	*/
	retval = usb_register(&usb_drive);
	if(retval < 0){
				
		printk("err :: %d :usb registration failed.....\n", retval);
		return -1;
	}

	printk(KERN_INFO "usb driver module initialized....\n");
	printk(KERN_INFO "USB device name is %s\n",usb_drive.name);
	return retval;
}

static void __exit usb_driver_exit(void){

	usb_deregister(&usb_drive);
	
	printk(KERN_INFO "Exiting from usb driver module....\
		n");
}

module_init(usb_driver_init);
module_exit(usb_driver_exit);
/*
	way of registering struct usb_driver{}...
		module_usb_driver(usb_drive);

	in this case there is no need of writing module_init()....
		and module_exit();
*/

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("wharish05@gmail.com");
MODULE_DESCRIPTION("USB driver using URB....");
