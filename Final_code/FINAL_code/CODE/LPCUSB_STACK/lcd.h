#ifndef __LCD_H
#define __LCD_H
#include "LPC17xx.h"

#define EN 22
#define RW 23
#define RS 24

#define LCD_DATA LPC_GPIO2
#define LCD_COM LPC_GPIO1

#define D4 4
#define D5 5
#define D6 6
#define D7 7

#define LCD_CLEAR 0X01

#define LCD_ENTRY_MODE 0X06

#define LCD_DISPLAY_ON 0X0C

#define LCD_FUNC_SET 0X28

#define LCD_LEFT_SHIFT_DISPLAY 0X18

#define LCD_LINE1 0X80
#define LCD_LINE2 0XC0

void lcd_init();
void lcd_busy();
void lcd_write_cmd(uint8_t cmdval);
void lcd_write_data(uint8_t cmdval);
void lcd_write_nibble(uint8_t cmd);
void lcd_puts(uint8_t cmd,char data[]);
#endif